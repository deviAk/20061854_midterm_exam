#pragma strict

private var power = 1200;
var bullet : Transform;
var target : Transform;
var spPoint : Transform;
var explosion: Transform;
var snd : AudioClip;

private var ftime : float = 0.0; 

function Start () {

}

function Update () {
        
	transform.LookAt(target);
	ftime += Time.deltaTime;
	
	var hit : RaycastHit;
    var fwd = transform.TransformDirection(Vector3.forward);
    
    Debug.DrawRay(spPoint.transform.position, fwd * 20, Color.green);
    if (Physics.Raycast(spPoint.transform.position, fwd, hit, 20) == false) return;
    Debug.Log(hit.collider.gameObject.name);
    if (hit.collider.gameObject.tag != "TANK" || ftime < 2) return;

    Instantiate(explosion, spPoint.transform.position, spPoint.transform.rotation);
    var obj = Instantiate(bullet, spPoint.transform.position, spPoint.transform.rotation);
    obj.rigidbody.AddForce(fwd * power);
    AudioSource.PlayClipAtPoint(snd, spPoint.transform.position);
    ftime = 0;
}
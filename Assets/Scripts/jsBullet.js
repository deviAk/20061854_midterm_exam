#pragma strict

var snd : AudioClip;
var explosion : Transform;

function Start () {

}

function Update () {

}

function OnTriggerEnter(coll : Collider) {
        
    Instantiate(explosion, this.transform.position - this.transform.forward, Quaternion.identity); 
	AudioSource.PlayClipAtPoint(snd, transform.position);
	Destroy(gameObject);
	
	if (coll.gameObject.tag == "BUILDING") {
		Destroy(coll.transform.root.gameObject);	
	}
	else if(coll.gameObject.tag == "ENEMY") {
		jsScore.hit++;
        if (jsScore.hit > 5) {
        	Destroy(coll.transform.root.gameObject);
        	Debug.Log(coll.transform.root.gameObject.name);
        	Application.LoadLevel("WinGame");
        }     
	}
	else if(coll.gameObject.tag == "TANK") {
		jsScore.lose++;
        if (jsScore.lose > 5) {
        	Application.LoadLevel("LostGame");
        }
	}
	
} 